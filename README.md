# NNet

Neural network library in Java from 15~20 years ago.


These days, we have so many open source "deep learning" libraries, but at the time there was really none as far as I can recall.

NNet was probably one of the very few, if not the only, open source artificial neural network libraries in Java ca 2000.
(I believe it was open-sourced on SourceForge.)


I'm not sure the quality of this library at this point though.

