//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
VonMisesActivation is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class VonMisesActivation extends Activation {
	
    public VonMisesActivation() {
        weightList = new ArrayList();
    }

    public VonMisesActivation(int dim) {
        weightList = new ArrayList();
        for(int i=0;i<dim;i++) {
            weightList.add(new DoubleVariable());
        }
    }
  
    public VonMisesActivation(int dim, double min, double max) {
        weightList = new ArrayList();
        Random rand = new Random();

        for(int i=0;i<dim;i++) {
            double v = (max-min)*rand.nextDouble() + min;
            weightList.add(new DoubleVariable(v));
        }
    }
  
    public VonMisesActivation(List newWeights) {
        weightList = newWeights;
    }
  
    public void initializeWeightList() {
        Iterator wit = weightList.iterator();
        while(wit.hasNext()) {
            Variable wvar = (Variable) wit.next();
            wvar.copyVariable(new DoubleVariable(0.0));
        }
    }
  
  
    public Variable process(List inputVars)  throws DimensionMismatchException {
      
        if(weightList.size() != inputVars.size()) {
            throw new DimensionMismatchException();
        }
    
        double  sum = 0.0;
        Iterator wit = weightList.iterator();
        Iterator iit = inputVars.iterator();
        while(wit.hasNext()) {
            Variable wvar = (Variable) wit.next();
            Variable ivar = (Variable) iit.next();
            sum += wvar.doubleValue() * ivar.angularValue();
        }
      
        sum /= Math.sqrt(weightList.size());
        double ret = NMath.vonMises(sum);
      
        return new RateVariable(ret);
    }
  
    
    public static void main(String[] args) {
      
        Activation act = new VonMisesActivation(1, -Math.PI, Math.PI);
        //System.out.println(act.getWeightDimension());
        //act.printWeightList();
      
        List inputVars = new ArrayList();
        Random rand = new Random(234343243);
        for(int i=0;i<act.getWeightDimension();i++) {
            double v = 2.0*Math.PI*rand.nextDouble() - Math.PI;
            inputVars.add(new AngularVariable(v));
        }
      
        Variable outputVar = new RateVariable();
        try {
            outputVar = act.process(inputVars);
        } catch(DimensionMismatchException ex) {
            System.out.println("testing...");
        }
        System.out.println(outputVar.rateValue());
    }
}
