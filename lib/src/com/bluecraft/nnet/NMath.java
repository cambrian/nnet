//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

/**
Miscellaneous math functions used in NNet package.

@author  Hyoungsoo Yoon
@version  0.2
*/
public class NMath {
	
    public static double tanh(double x) {
        return (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x));
    }
    public static double tanh(double x, double a) {
        return (Math.exp(a*x) - Math.exp(-a*x)) / (Math.exp(a*x) + Math.exp(-a*x));
    }

    public static double sigmoid(double x) {
        return 1.0 / (1.0 + Math.exp(-x));
    }
    public static double sigmoid(double x, double a) {
        return 1.0 / (1.0 + Math.exp(-a*x));
    }
	
    public static double gaussian(double x) {
        return Math.exp(-0.5*x*x);
    }
    public static double gaussian(double x, double a) {
        return Math.exp(-0.5 * (x*x)/(a*a));
    }
	
    public static double vonMises(double x) {
        return Math.exp(Math.cos(x) - 1.0);
    }
    public static double vonMises(double x, double a) {
        return Math.exp((Math.cos(x) - 1.0) / (a*a));
    }
	
    public static void main(String[] args) {

        System.out.println(tanh(-1.0));
        System.out.println(tanh(0.0));
    	System.out.println(tanh(1.0));

        System.out.println(sigmoid(-1.0));
        System.out.println(sigmoid(0.0));
    	System.out.println(sigmoid(1.0));
    	
        System.out.println(gaussian(-1.0));
        System.out.println(gaussian(0.0));
        System.out.println(gaussian(1.0));
        
        System.out.println(vonMises(-1.0));
        System.out.println(vonMises(0.0));
        System.out.println(vonMises(1.0));
    }
}
