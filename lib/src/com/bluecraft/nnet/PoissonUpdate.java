//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
PoissonUpdate is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class PoissonUpdate extends Update {

    protected long meanInterval = 0;
    protected long delay = 0;
    Random rand = new Random();
    
    public PoissonUpdate() {
    }
    public PoissonUpdate(long m) {
        meanInterval = m;
    }
    public PoissonUpdate(long m, long d) {
        meanInterval = m;
        delay = d;
    }
    
    
    public long nextInterval() {
        // temporary
        long interval = (long) (meanInterval * Math.log(1.0/(1.0 - rand.nextDouble())));
        return interval;
    }
    public long nextDelay() {
        return delay;
    }
	
	
    public static void main(String[] args) {
        Update updt = new PoissonUpdate(300);
        while(true) {
            System.out.println(updt.nextInterval());
            try {
                Thread.sleep(500);
            } catch(InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
	
}
