//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
TanhNeuron is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class TanhNeuron extends Neuron {

    public TanhNeuron() {
        activationFunction = new TanhActivation();
        updateSchedule = new PoissonUpdate();
    }
    public TanhNeuron(long meanInterval) {
        activationFunction = new TanhActivation();
        updateSchedule = new PoissonUpdate(meanInterval);
    }
    
    public TanhNeuron(int preDim) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new TanhActivation(preDim);
        updateSchedule = new PoissonUpdate();
    }
    public TanhNeuron(int preDim, long meanInterval) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new TanhActivation(preDim);
        updateSchedule = new PoissonUpdate(meanInterval);
    }
    
    public TanhNeuron(int preDim, double min, double max) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new TanhActivation(preDim, min, max);
        updateSchedule = new PoissonUpdate();
    }
    public TanhNeuron(int preDim, double min, double max, long meanInterval) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new TanhActivation(preDim, min, max);
        updateSchedule = new PoissonUpdate(meanInterval);
    }
    
    public TanhNeuron(int preDim, Set postNeurons) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        Iterator nit = postNeurons.iterator();
        while(nit.hasNext()) {
            postSynapticNeuronSet.add((Neuron) nit.next());
        }
        activationFunction = new TanhActivation(preDim);
        updateSchedule = new PoissonUpdate();
    }
    public TanhNeuron(int preDim, Set postNeurons, long meanInterval) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        Iterator nit = postNeurons.iterator();
        while(nit.hasNext()) {
            postSynapticNeuronSet.add((Neuron) nit.next());
        }
        activationFunction = new TanhActivation(preDim);
        updateSchedule = new PoissonUpdate(meanInterval);
    }
    
    
    public void run() {
        while(true) {
            scheduledFire();
            Variable outputVar = getPostSynapticVariable();
            System.out.println(outputVar.doubleValue());
            yield();
        }
    }
    

    public static void main(String[] args) {
        
        //synch mode        
        final int inputDim = 10;
        final double min = -1.0;
        final double max = 1.0;
        Neuron perceptron = new TanhNeuron(inputDim, min, max);
        
        List inputVariableList = new ArrayList();
        Random rand = new Random();
        //Update regularSleep = new ConstantUpdate(200);
        
        while(true) {
            inputVariableList.clear();
            for(int i=0;i<inputDim;i++) {
                double v = 2.0*rand.nextDouble() - 1.0;
                inputVariableList.add(new DoubleVariable(v));
            }
            perceptron.setPreSynapticVariableList(inputVariableList);
            perceptron.fire(500,100);
            Variable outputVar = perceptron.getPostSynapticVariable();
            System.out.println(outputVar.doubleValue());
            //regularSleep.sleepInterval();
        }

// asynch mode
/*
        final int inputDim = 10;
        final double min = -1.0;
        final double max = 1.0;
        Neuron perceptron = new TanhNeuron(inputDim, min, max, 900L);
        
        List inputVariableList = new ArrayList();
        Random rand = new Random();
        for(int i=0;i<inputDim;i++) {
            double v = 2.0*rand.nextDouble() - 1.0;
            inputVariableList.add(new DoubleVariable(v));
        }
        perceptron.setPreSynapticVariableList(inputVariableList);
    
        ///
        perceptron.start();
        ///
        
        while(true) {
            inputVariableList.clear();
            for(int i=0;i<inputDim;i++) {
                double v = 2.0*rand.nextDouble() - 1.0;
                inputVariableList.add(new DoubleVariable(v));
            }
            perceptron.setPreSynapticVariableList(inputVariableList);
            try {
                sleep(1333);
            } catch(InterruptedException ex) {
                ex.printStackTrace();
            }
        }
*/

    }
}
