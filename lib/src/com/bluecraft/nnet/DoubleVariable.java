//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
DoubleVariable is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class DoubleVariable extends Variable {	
	
    protected double  value;

  
    public DoubleVariable() { 
        value = 0.0;
    }
	
    public DoubleVariable(double v) { 
        value = v;
    }
	
  
    public int bipolarValue() {
    	return (int) value;
    }
	
    public double rateValue() {
    	return (value>=0) ? value : 0.0;
    }
	
    public double doubleValue() {
        return value;
    }
	
    public double angularValue() {
        return value;
    }
	
    public void copyVariable(Variable cpVar) {
    	value = cpVar.doubleValue();
    }
	
	
    public static void main(String[] args) {
        Variable var = new DoubleVariable(3.0);
        System.out.println(var.toString());
        System.out.println(var.rateValue());
    }
	
}
