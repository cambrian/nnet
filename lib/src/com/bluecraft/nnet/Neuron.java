//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;

/**
Neuron.
Runs in both asynchronous and synchronous modes.

@author Hyoungsoo Yoon
@version 0.2
*/
public abstract class Neuron extends Thread {

    protected boolean bSynchronized = false;

    protected List preSynapticVariableList = null;
    protected Set postSynapticNeuronSet = null;
    protected Variable internalVariable = null;
    protected Variable postSynapticVariable = null;
    protected Activation activationFunction = null;
    protected Update updateSchedule = null;
	
    public Neuron() {
	preSynapticVariableList = new ArrayList();
        postSynapticNeuronSet = new HashSet();
    }

    public void setPreSynapticVariableList(List inputVars) {
        preSynapticVariableList = inputVars;
    }
  
    public Variable getInternalVariable() {
        return internalVariable;
    }
  
    public Variable getPostSynapticVariable() {
        return postSynapticVariable;
    }
    
    public void refreshPostSynapticVariable() {
        postSynapticVariable = internalVariable;
    }
  
    public void activate() {
        try {
            internalVariable = activationFunction.process(preSynapticVariableList);
        } catch(DimensionMismatchException ex) {
            ex.printStackTrace();
        }
    }

    public void fire() {
        activate();
        refreshPostSynapticVariable();
    }
    
    public void fire(long interval) {
        try {
            sleep(interval);
        } catch(InterruptedException ex) {
            ex.printStackTrace();
        }
        activate();
        refreshPostSynapticVariable();
    }
    
    public void fire(long interval, long delay) {
        try {
            sleep(interval);
            activate();
            sleep(delay);
            refreshPostSynapticVariable();
        } catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    public void scheduledFire() {
        try {
            sleep(updateSchedule.nextInterval());
            activate();
            sleep(updateSchedule.nextDelay());
            refreshPostSynapticVariable();
        } catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    
    public void run() {
        while(true) {
            scheduledFire();
            yield();
        }
    }

}
