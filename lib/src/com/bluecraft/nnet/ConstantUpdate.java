//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
ConstantUpdate is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class ConstantUpdate extends Update {

    protected long interval = 0;
    protected long delay = 0;
    
    public ConstantUpdate() {
    }
    public ConstantUpdate(long i) {
        interval = i;
    }
    public ConstantUpdate(long i, long d) {
        interval = i;
        delay = d;
    }
        
    public long nextInterval() {
        return interval;
    }
    public long nextDelay() {
        return delay;
    }


    public static void main(String[] args) {
        Update updt = new ConstantUpdate(300);
        while(true) {
            System.out.println(updt.nextInterval());
            try {
                Thread.sleep(500);
            } catch(InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
	
}
