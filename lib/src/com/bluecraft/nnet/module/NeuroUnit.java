//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
 */
package com.bluecraft.nnet.module;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import com.bluecraft.nnet.*;

         
public class NeuroUnit extends Canvas implements Serializable {
                     
                     

    //Constructor sets inherited properties
    public NeuroUnit(){
        setSize(100,60);
        setBackground(Color.red);
    }

	
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(
                                     UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }

        //Create the top-level container and add contents to it.
        JFrame frame = new JFrame("NeuroUnit");
        NeuroUnit unit = new NeuroUnit();
        frame.getContentPane().add(unit, BorderLayout.CENTER);

        //Finish setting up the frame, and show it.
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        frame.pack();
        frame.setVisible(true);
    }
}
