//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
 */
package com.bluecraft.nnet.module;

import java.util.*;
import com.bluecraft.nnet.*;


public class MultiLayerPerceptron extends NeuroModule {
	
	
    public static void main(String[] args) {

        final int dim = 3;
		
        Neuron output = new OutputNeuron();
        Neuron[] inputArray = new InputNeuron[dim];
		
		
		
        System.out.println("SimpleTanhUnit");
    }
}


class InputNeuron extends LinearNeuron {

    public InputNeuron() {
        postSynapticNeuronSet = null;
        preSynapticVariableList = null;
        postSynapticVariable = new RateVariable();
        activationFunction = new LinearActivation();
    }

    public void activate() {
  
        try {
            postSynapticVariable = activationFunction.process(preSynapticVariableList);
        } catch(DimensionMismatchException ex) {
            System.out.println("testing...");
        }
  
    }
  
}

class OutputNeuron extends TanhNeuron {

    public OutputNeuron() {
        postSynapticNeuronSet = null;
        preSynapticVariableList = null;
        postSynapticVariable = new RateVariable();
        activationFunction = new TanhActivation();
    }

    public void activate() {
  
        try {
            postSynapticVariable = activationFunction.process(preSynapticVariableList);
        } catch(DimensionMismatchException ex) {
            System.out.println("testing...");
        }
  
    }
  
}
