//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Neural-net Software Development Kit in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
NNet: Neural-net Software Development Kit in Java.

@copyright  Hyoungsoo Yoon
@date  May 21st, 1999
*/
package com.bluecraft.nnet;

import java.util.*;


/**
ThresholdNeuron is ...

@author  Hyoungsoo Yoon
@version  0.2
*/
public class ThresholdNeuron extends Neuron {

    public ThresholdNeuron() {
        activationFunction = new ThresholdActivation();
    }
    
    public ThresholdNeuron(int preDim) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new ThresholdActivation(preDim);
    }
    
    public ThresholdNeuron(int preDim, double min, double max) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        activationFunction = new ThresholdActivation(preDim, min, max);
    }
    
    public ThresholdNeuron(int preDim, Set postNeurons) {
        for(int i=0;i<preDim;i++) {
            preSynapticVariableList.add(new DoubleVariable());
        }
        Iterator nit = postNeurons.iterator();
        while(nit.hasNext()) {
            postSynapticNeuronSet.add((Neuron) nit.next());
        }
        activationFunction = new ThresholdActivation(preDim);
    }
    

    public static void main(String[] args) {
        
   
        final int inputDim = 10;
        final double min = -1.0;
        final double max = 1.0;
        Neuron perceptron = new ThresholdNeuron(inputDim, min, max);
        
        ///
        List inputVariableList = new ArrayList();
        Random rand = new Random(343897624);
        for(int i=0;i<inputDim;i++) {
            double v = 2.0*rand.nextDouble() - 1.0;
            inputVariableList.add(new DoubleVariable(v));
        }
        perceptron.setPreSynapticVariableList(inputVariableList);
    
        perceptron.activate();
        Variable outputVar = perceptron.getPostSynapticVariable();
        ///
     
        System.out.println(outputVar.bipolarValue());
    }
}
